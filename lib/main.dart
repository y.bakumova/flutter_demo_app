import 'dart:async';
import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_assets.dart';
import 'package:scale3c_flutter_test_task/presentation/app.dart';

import 'firebase_options.dart';

void main() async {
  await runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      );
      Future.wait([
        for (String svgFile in AppAssets.cachedImages)
          precachePicture(
            ExactAssetPicture(SvgPicture.svgStringDecoderBuilder, svgFile),
            null,
          ),
      ]).whenComplete(
        () => runApp(const App()),
      );
    },
    (error, stackTrace) => log(error.toString()),
  );
}

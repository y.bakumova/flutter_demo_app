import 'package:firebase_auth/firebase_auth.dart';

enum AuthStatus {
  successful,
  userNotFound,
  userDisabled,
  invalidEmail,
  wrongPassword,
  weakPassword,
  emailAlreadyExists,
  cancelledByUser,
  unknown,
}

class FirebaseAuthExceptionHandler {
  static handleAuthException(FirebaseAuthException e) {
    AuthStatus status;
    switch (e.code) {
      case 'user-not-found':
        status = AuthStatus.userNotFound;
        break;
      case 'user-disabled':
        status = AuthStatus.userDisabled;
        break;
      case 'invalid-email':
        status = AuthStatus.invalidEmail;
        break;
      case 'wrong-password':
        status = AuthStatus.wrongPassword;
        break;
      case 'weak-password':
        status = AuthStatus.weakPassword;
        break;
      case 'email-already-in-use':
        status = AuthStatus.emailAlreadyExists;
        break;
      default:
        status = AuthStatus.unknown;
    }
    return status;
  }

  static String generateErrorMessage(error) {
    String errorMessage;
    switch (error) {
      case AuthStatus.userNotFound:
        errorMessage = 'No user found for that email.';
        break;
      case AuthStatus.userDisabled:
        errorMessage =
            'The user corresponding to the given email has been disabled.';
        break;
      case AuthStatus.invalidEmail:
        errorMessage = 'Your email address appears to be malformed.';
        break;
      case AuthStatus.weakPassword:
        errorMessage = 'The password provided is too weak.';
        break;
      case AuthStatus.wrongPassword:
        errorMessage = 'Wrong password provided for that user.';
        break;
      case AuthStatus.emailAlreadyExists:
        errorMessage =
            'The email address is already in use by another account.';
        break;
      case AuthStatus.cancelledByUser:
        errorMessage =
            'Authentification process is cancelled by the user.';
        break;
      default:
        errorMessage = 'An error occured. Please try again later.';
    }
    return errorMessage;
  }
}

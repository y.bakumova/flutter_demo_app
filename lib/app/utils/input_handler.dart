import '../constants/app_strings.dart';

mixin InputHandlerMixin {
  bool isEmpty(String? value) {
    if (value == null || value.isEmpty) {
      return true;
    }
    return false;
  }

  String? isEmailValid(String? email) {
    const String pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    final RegExp regex = RegExp(pattern);

    if (isEmpty(email)) {
      return AppStrings.enterText;
    } else if (!regex.hasMatch(email!)) {
      return AppStrings.enterCorrectEmail;
    }
    return null;
  }

  String? isPasswordValid(String? password) {
    const String pattern = r'^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{8,}$';
    final RegExp regex = RegExp(pattern);

    if (isEmpty(password)) {
      return AppStrings.enterText;
    } else if (password!.length < 8) {
      return AppStrings.shortPassword;
    } else if (!regex.hasMatch(password)) {
      return AppStrings.incorrectPassword;
    }
    return null;
  }

  String? passwordsMatch(String newPass, String repeatPass) =>
      newPass != repeatPass ? AppStrings.passwordsDoNotMatch : null;
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/exeptions/firebase_auth_exeption.dart';

mixin ExeptionHandlerMixin {
  void authHandler({
    required AuthStatus status,
    required VoidCallback onSuccess,
  }) {
    if (status == AuthStatus.successful) {
      onSuccess();
    } else {
      final message = FirebaseAuthExceptionHandler.generateErrorMessage(status);
      Get.showSnackbar(
        GetSnackBar(
          message: message,
          duration: const Duration(seconds: 3),
        ),
      );
    }
  }
}

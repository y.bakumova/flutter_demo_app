import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';

class AppTheme {
  static final theme = ThemeData(
    scaffoldBackgroundColor: AppColors.white,
    fontFamily: GoogleFonts.roboto().fontFamily,
    textTheme: TextTheme(
      headline1: TextStyle(
        fontSize: 24.sp,
        fontWeight: FontWeight.w500,
        color: AppColors.gray60,
        fontFamily: GoogleFonts.ubuntu().fontFamily,
      ),
      bodyText1: TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w500,
        color: AppColors.gray50,
      ),
      caption: TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w900,
        color: AppColors.gray50,
      ),
      button: TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w900,
        color: AppColors.peach,
        decoration: TextDecoration.underline,
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: AppColors.white,
        backgroundColor: AppColors.green,
        textStyle: TextStyle(
          fontSize: 16.sp,
          fontWeight: FontWeight.w900,
        ),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        foregroundColor: AppColors.gray50,
        backgroundColor: AppColors.white,
        side: const BorderSide(color: AppColors.gray40),
        textStyle: TextStyle(
          fontSize: 16.sp,
          fontWeight: FontWeight.w900,
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      isDense: true,
      fillColor: AppColors.gray10,
      labelStyle: TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w500,
        color: AppColors.gray40,
      ),
      border: InputBorder.none,
      errorBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      errorMaxLines: 2,
      errorStyle: TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: AppColors.error,
      ),
    ),
  );
}

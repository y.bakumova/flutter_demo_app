import 'package:flutter/material.dart';

class AppColors {
  static const Color error = Color.fromRGBO(175, 58, 58, 1);
  static const Color green = Color.fromRGBO(32, 195, 175, 1);
  static const Color peach = Color.fromRGBO(255, 177, 157, 1);
  static const Color white = Color.fromRGBO(255, 255, 255, 1);
  static const Color gray10 = Color.fromRGBO(247, 247, 247, 1);
  static const Color gray40 = Color.fromRGBO(176, 176, 195, 1);
  static const Color gray50 = Color.fromRGBO(131, 131, 145, 1);
  static const Color gray60 = Color.fromRGBO(82, 84, 100, 1);
  static const Color grayBackground = Color.fromRGBO(82, 84, 100, 1);
}

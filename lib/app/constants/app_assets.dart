class AppAssets {
  static const String images = 'assets/images/';
  static const String icons = 'assets/icons/';

  static const String signIn = '${images}sign_in_image.svg';
  static const String signUp = '${images}sign_up_image.svg';
  static const String menu = '${icons}menu_icon.svg';
  static const String facebook = '${icons}facebook_icon.svg';
  static const String twitter = '${icons}twitter_icon.svg';
  static const String linkedIn = '${icons}linkedin_icon.svg';
  static const String phone = '${icons}phone_icon.svg';
  static const String email = '${icons}email_icon.svg';
  static const String completedProjects = '${icons}completed_icon.svg';
  static const String defaultProfileImage = '${images}default_image.svg';

  static const cachedImages = [
    signIn,
    signUp,
    menu,
    facebook,
    twitter,
    linkedIn,
    phone,
    email,
    completedProjects,
    defaultProfileImage,
  ];
}

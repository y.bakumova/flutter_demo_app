class AppStrings {
  static const String signIn = 'Sign In';
  static const String signUp = 'Sign Up';
  static const String userName = 'Username';
  static const String password = 'Password';
  static const String forgotPassword = 'Forgot your password?';
  static const String login = 'Login';
  static const String or = 'or';
  static const String dontHaveAccount = 'Don\'t have an account? ';
  static const String alreadyHaveAccount = 'Already have an account? ';
  static const String enterEmail = 'Enter email';
  static const String enterPassword = 'Enter password';
  static const String confirmPassword = 'Confirm password';
  static const String edit = 'Edit';
  static const String aboutMe = 'About Me';
  static const String phoneNumber = 'Phone number';
  static const String email = 'Email';
  static const String completedProjects = 'Completed projects';
  static const String logOut = 'Log Out';
  static const String logOutConfirmation = 'Are You Logging Out?';
  static const String stayInApp = 'No, stay in app';
  static const String confirmLogOut = 'Yes, log out';
  static const String comeBackSoon = 'Come back soon!';

  static const String notSpeciifed = 'not specified';
  static const String locationNotSpeciifed = 'No Location';
  static const String enterText = 'Please fill in this field';
  static const String enterCorrectEmail = 'Please enter correct email';
  static const String shortPassword = 'Password must be at least 8 characters';
  static const String incorrectPassword =
      'Password must have at least 1 letter and 1 digit, without special characters';
  static const String passwordsDoNotMatch = 'Passwords do not match';
  static const String noDataFound = 'No data found';
  static const String errorOccured =
      'The error occured. Please try again later!';
}

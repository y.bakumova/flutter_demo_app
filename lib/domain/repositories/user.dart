import 'package:scale3c_flutter_test_task/data/models/user/user.dart';

abstract class UserRepository {
  Future<UserModel> get();

  Future<void> set(UserModel user);
}

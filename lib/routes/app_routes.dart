class AppRoutes {
  static const home = '/';
  static const auth = '/auth';
  static const profile = '/profile';
}

import 'package:get/get.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/auth/auth_binding.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/user/user_binding.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/auth/auth_page.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/home/home_page.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/profile/profile_page.dart';

import 'app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.home,
      page: () => const HomePage(),
    ),
    GetPage(
      name: AppRoutes.auth,
      page: () => const AuthPage(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: AppRoutes.profile,
      page: () => const ProfilePage(),
      binding: UserBinding(),
    ),
  ];
}

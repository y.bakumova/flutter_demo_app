import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:scale3c_flutter_test_task/data/models/user/user.dart';
import 'package:scale3c_flutter_test_task/domain/repositories/user.dart';

class FirestoreUserRepository extends UserRepository {
  static const String _usersKey = 'users';

  final _reference = FirebaseFirestore.instance.collection(_usersKey);
  final _user = FirebaseAuth.instance.currentUser;

  @override
  Future<UserModel> get() async {
    final snapshot = await _reference.doc(_user!.uid).get();
    return UserModel.fromJson(snapshot.data()!);
  }

  @override
  Future<void> set(UserModel user) async =>
      await _reference.doc(user.id).set(user.toJson());
}

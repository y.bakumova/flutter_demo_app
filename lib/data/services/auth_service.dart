import 'package:firebase_auth/firebase_auth.dart';
import 'package:scale3c_flutter_test_task/app/core/exeptions/exeption.dart';
import 'package:scale3c_flutter_test_task/app/core/exeptions/firebase_auth_exeption.dart';

class AuthService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  late AuthStatus _status;

  User? get user => _firebaseAuth.currentUser;

  Future<AuthStatus> signUp(String email, String password) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      _status = AuthStatus.successful;
    } on FirebaseAuthException catch (e) {
      _status = FirebaseAuthExceptionHandler.handleAuthException(e);
    } on Object catch (e) {
      _status = AuthStatus.unknown;
      throw AuthException(message: e.toString());
    }
    return _status;
  }

  Future<AuthStatus> signIn(String email, String password) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      _status = AuthStatus.successful;
    } on FirebaseAuthException catch (e) {
      _status = FirebaseAuthExceptionHandler.handleAuthException(e);
    } on Object catch (e) {
      _status = AuthStatus.unknown;
      throw AuthException(message: e.toString());
    }
    return _status;
  }

  Future<void> signOut() async {
    try {
      await _firebaseAuth.signOut();
    } on Object catch (e) {
      throw AuthException(message: e.toString());
    }
  }
}

class AuthException extends DocumentedException {
  AuthException({String? message, Object? cause})
      : super(message: message, cause: cause);
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_assets.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;

  const CustomAppBar({
    this.title,
    Key? key,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(60.h);

  @override
  Widget build(context) {
    return AppBar(
      elevation: 0,
      centerTitle: true,
      foregroundColor: AppColors.gray60,
      backgroundColor: AppColors.white,
      title: title != null
          ? Text(
              title!,
              style: TextStyle(
                fontSize: 18.sp,
                fontWeight: FontWeight.w900,
              ),
            )
          : null,
      actions: [
        Builder(
          builder: (context) => IconButton(
            onPressed: () => Scaffold.of(context).openEndDrawer(),
            icon: SvgPicture.asset(
              AppAssets.menu,
              fit: BoxFit.scaleDown,
            ),
          ),
        ),
      ],
    );
  }
}

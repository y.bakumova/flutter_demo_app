import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_strings.dart';

class ProfileCard extends StatelessWidget {
  final String icon;
  final String title;
  final String? data;

  const ProfileCard({
    super.key,
    required this.icon,
    required this.title,
    required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.h,
      padding: EdgeInsets.symmetric(horizontal: 24.w, vertical: 16.h),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.gray10),
      ),
      child: Row(
        children: [
          SvgPicture.asset(
            icon,
            width: 20.w,
            height: 20.h,
            color: AppColors.peach,
          ),
          24.horizontalSpace,
          const VerticalDivider(
            color: AppColors.gray10,
            thickness: 1,
          ),
          24.horizontalSpace,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Text(
                data ?? AppStrings.notSpeciifed,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: AppColors.white),
              ),
            ],
          )
        ],
      ),
    );
  }
}

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SocialLoginButton extends StatelessWidget {
  final String icon;
  final VoidCallback onTap;

  const SocialLoginButton({
    Key? key,
    required this.icon,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100.w,
      height: 60.h,
      child: OutlinedButton(
        onPressed: onTap,
        child: SvgPicture.asset(
          icon,
          width: 18.w,
          height: 18.h,
        ),
      ),
    );
  }
}

// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_strings.dart';
import 'package:scale3c_flutter_test_task/app/utils/input_handler.dart';

class EmailForm extends StatelessWidget with InputHandlerMixin {
  final TextEditingController controller;
  final String label;

  const EmailForm(
    this.controller, {
    this.label = AppStrings.enterEmail,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: AppColors.gray40,
      controller: controller,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.emailAddress,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: isEmailValid,
      decoration: InputDecoration(
        labelText: label,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';
import 'package:scale3c_flutter_test_task/app/utils/input_handler.dart';

class PasswordForm extends StatefulWidget {
  final String label;
  final TextEditingController controller;
  final String? Function(String?)? validator;

  const PasswordForm(
    this.controller, {
    this.validator,
    required this.label,
    Key? key,
  }) : super(key: key);

  @override
  State<PasswordForm> createState() => _PasswordFormState();
}

class _PasswordFormState extends State<PasswordForm> with InputHandlerMixin {
  bool isVisible = false;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: AppColors.gray40,
      controller: widget.controller,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.visiblePassword,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: widget.validator ?? isPasswordValid,
      obscureText: isVisible ? false : true,
      decoration: InputDecoration(
        labelText: widget.label,
        suffixIcon: IconButton(
          onPressed: () => setState(() => isVisible = !isVisible),
          color: AppColors.gray40,
          iconSize: 25.sm,
          icon: Icon(
            isVisible
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined,
          ),
        ),
      ),
    );
  }
}

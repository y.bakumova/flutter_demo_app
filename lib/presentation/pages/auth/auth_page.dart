// ignore_for_file: unused_import

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_assets.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_strings.dart';
import 'package:scale3c_flutter_test_task/app/utils/input_handler.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/auth/auth_controller.dart';
import 'package:scale3c_flutter_test_task/presentation/widgets/auth/email_form.dart';
import 'package:scale3c_flutter_test_task/presentation/widgets/auth/password_form.dart';
import 'package:scale3c_flutter_test_task/presentation/widgets/custom_app_bar.dart';
import 'package:scale3c_flutter_test_task/presentation/widgets/auth/social_login_button.dart';

class AuthPage extends GetView<AuthController> with InputHandlerMixin {
  const AuthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final emailController = TextEditingController();
    final passwordController = TextEditingController();
    final confirmPasswordController = TextEditingController();

    return Obx(
      () => Scaffold(
        appBar: CustomAppBar(
          title: controller.appBarTitle,
        ),
        endDrawer: const SizedBox(),
        body: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 30.w),
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  36.verticalSpace,
                  SvgPicture.asset(
                    controller.image,
                    height: 145.h,
                    fit: BoxFit.contain,
                  ),
                  36.verticalSpace,
                  EmailForm(
                    emailController,
                    label: controller.emailFormLabel,
                  ),
                  16.verticalSpace,
                  PasswordForm(
                    passwordController,
                    label: controller.passwordFormLabel,
                  ),
                  16.verticalSpace,
                  controller.isSignIn.value
                      ? _forgotPasswordButton(context)
                      : PasswordForm(
                          confirmPasswordController,
                          label: AppStrings.confirmPassword,
                          validator: (value) =>
                              passwordsMatch(value!, passwordController.text) ??
                              isPasswordValid(value),
                        ),
                  16.verticalSpace,
                  SizedBox(
                    height: 60,
                    child: TextButton(
                      child: Text(controller.buttonTitle),
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {
                          await controller.onSubmit(
                            emailController.text.trim(),
                            passwordController.text,
                          );
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.h),
                    child: Text(
                      AppStrings.or,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SocialLoginButton(
                        icon: AppAssets.facebook,
                        onTap: () => controller.onSignInWithFacebook(),
                      ),
                      SocialLoginButton(
                        icon: AppAssets.twitter,
                        onTap: () => controller.onSignInWithTwitter(),
                      ),
                      SocialLoginButton(
                        icon: AppAssets.linkedIn,
                        onTap: () => controller.onSignInWithLinkedIn(),
                      ),
                    ],
                  ),
                  50.verticalSpace,
                  _switchModeButton(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _forgotPasswordButton(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 31.h),
      alignment: Alignment.centerRight,
      child: InkWell(
        onTap: () {},
        child: Text(
          AppStrings.forgotPassword,
          style: Theme.of(context).textTheme.caption,
        ),
      ),
    );
  }

  Widget _switchModeButton(BuildContext context) {
    return TextButton(
      onPressed: controller.changeMode,
      style: TextButton.styleFrom(
        backgroundColor: AppColors.white,
      ),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: controller.switchDescription,
          style: Theme.of(context).textTheme.caption,
          children: <TextSpan>[
            TextSpan(
              text: controller.switchTitle,
              style: Theme.of(context).textTheme.button,
            ),
          ],
        ),
      ),
    );
  }
}

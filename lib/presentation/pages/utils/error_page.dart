import 'package:flutter/material.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_strings.dart';

class ErrorPage extends StatelessWidget {
  final String? error;

  const ErrorPage(
    this.error, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          error ?? AppStrings.errorOccured,
          style: Theme.of(context).textTheme.caption,
        ),
      ),
    );
  }
}

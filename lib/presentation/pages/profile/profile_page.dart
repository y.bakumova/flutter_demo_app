import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_assets.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_colors.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_strings.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/auth/auth_controller.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/user/user_controller.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/utils/error_page.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/utils/loading_page.dart';
import 'package:scale3c_flutter_test_task/presentation/widgets/custom_app_bar.dart';
import 'package:scale3c_flutter_test_task/presentation/widgets/profile_card.dart';

class ProfilePage extends GetView<UserController> {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => Scaffold(
        appBar: const CustomAppBar(),
        endDrawer: const SizedBox(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              _BuildProfileInfo(),
              _BuildProfileCards(),
            ],
          ),
        ),
      ),
      onLoading: const LoadingPage(),
      onEmpty: const ErrorPage(AppStrings.noDataFound),
      onError: (error) => ErrorPage(error),
    );
  }
}

class _BuildProfileInfo extends GetView<UserController> {
  const _BuildProfileInfo();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildAvatarWidget(),
          24.verticalSpace,
          Text(
            controller.user.value.username ?? AppStrings.userName,
            style: Theme.of(context).textTheme.headline1,
          ),
          10.verticalSpace,
          _buildLocationAndIdWidget(context),
          16.verticalSpace,
          InkWell(
            onTap: () {},
            child: Text(
              AppStrings.edit,
              style: Theme.of(context).textTheme.button,
            ),
          ),
          24.verticalSpace,
          _buildOptionButtons(),
        ],
      ),
    );
  }

  Widget _buildAvatarWidget() {
    final user = controller.user.value;
    final hasImage = user.imageUrl != null && user.imageUrl!.isNotEmpty;

    return CircleAvatar(
      maxRadius: 60.r,
      backgroundColor: AppColors.white,
      backgroundImage: hasImage ? NetworkImage(user.imageUrl!) : null,
      child: !hasImage
          ? SvgPicture.asset(
              AppAssets.defaultProfileImage,
              fit: BoxFit.fill,
            )
          : const SizedBox.shrink(),
    );
  }

  Widget _buildLocationAndIdWidget(BuildContext context) {
    final user = controller.user.value;

    return SizedBox(
      height: 30.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Text(
              user.location ?? AppStrings.locationNotSpeciifed,
              style: Theme.of(context).textTheme.bodyText1,
              textAlign: TextAlign.end,
              maxLines: 1,
            ),
          ),
          Container(
            height: 5.h,
            width: 5.h,
            margin: EdgeInsets.symmetric(horizontal: 10.w),
            decoration: const BoxDecoration(
              color: AppColors.gray50,
              shape: BoxShape.circle,
            ),
          ),
          Expanded(
            child: Text(
              'ID: ${user.id}',
              style: Theme.of(context).textTheme.bodyText1,
              maxLines: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildOptionButtons() {
    final AuthController authController = Get.find();

    return SizedBox(
      height: 60.h,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: OutlinedButton(
              onPressed: () {},
              child: const Text(AppStrings.aboutMe),
            ),
          ),
          16.horizontalSpace,
          Expanded(
            child: TextButton(
              onPressed: () => authController.onSignOut(),
              child: const Text(AppStrings.logOut),
            ),
          ),
        ],
      ),
    );
  }
}

class _BuildProfileCards extends GetView<UserController> {
  const _BuildProfileCards();

  @override
  Widget build(BuildContext context) {
    final user = controller.user.value;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 40.h, horizontal: 30.w),
      color: AppColors.grayBackground,
      child: Column(
        children: [
          ProfileCard(
            icon: AppAssets.phone,
            title: AppStrings.phoneNumber,
            data: user.phoneNumber,
          ),
          16.verticalSpace,
          ProfileCard(
            icon: AppAssets.email,
            title: AppStrings.email,
            data: user.email,
          ),
          16.verticalSpace,
          ProfileCard(
            icon: AppAssets.completedProjects,
            title: AppStrings.completedProjects,
            data: user.completedProjects?.toString(),
          ),
        ],
      ),
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/auth/auth_controller.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/user/user_controller.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/auth/auth_page.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/utils/loading_page.dart';
import 'package:scale3c_flutter_test_task/presentation/pages/profile/profile_page.dart';

class HomePage extends GetView<AuthController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => AuthController());

    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.active) {
          return const LoadingPage();
        }
        if (snapshot.hasData) {
          Get.lazyPut(() => UserController());
          return const ProfilePage();
        } else {
          return const AuthPage();
        }
      },
    );
  }
}

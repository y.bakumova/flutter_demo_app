import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_notifier.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_assets.dart';
import 'package:scale3c_flutter_test_task/app/constants/app_strings.dart';
import 'package:scale3c_flutter_test_task/app/utils/exeption_handler.dart';
import 'package:scale3c_flutter_test_task/data/models/user/user.dart';
import 'package:scale3c_flutter_test_task/data/repositories/user.dart';
import 'package:scale3c_flutter_test_task/data/services/auth_service.dart';

class AuthController extends GetxController
    with StateMixin, ExeptionHandlerMixin {
  final service = AuthService();
  final repository = FirestoreUserRepository();

  RxBool isSignIn = true.obs;
  RxBool isFirstSignIn = false.obs;

  @override
  void onInit() async {
    super.onInit();
    change(null, status: RxStatus.success());
  }

  void changeMode() => isSignIn.value = !isSignIn.value;

  Future<void> signIn(String email, String password) async {
    final status = await service.signIn(email, password);
    authHandler(
      status: status,
      onSuccess: () => isFirstSignIn.value = false,
    );
  }

  Future<void> signUp(String email, String password) async {
    final status = await service.signUp(email, password);
    authHandler(
      status: status,
      onSuccess: () async {
        UserModel user = UserModel(
          id: service.user!.uid,
          email: service.user!.email ?? '',
        );
        await repository.set(user);
      },
    );
  }

  void onSignInWithFacebook() {}

  void onSignInWithTwitter() {}

  void onSignInWithLinkedIn() {}

  Future<void> onSubmit(String email, String password) async => isSignIn.isTrue
      ? await signIn(email, password)
      : await signUp(email, password);

  Future<void> onSignOut() async => await service.signOut();

  String get image => isSignIn.isTrue ? AppAssets.signIn : AppAssets.signUp;

  String get appBarTitle =>
      isSignIn.isTrue ? AppStrings.signIn : AppStrings.signUp;

  String get buttonTitle =>
      isSignIn.isTrue ? AppStrings.login : AppStrings.signUp;

  String get emailFormLabel =>
      isSignIn.isTrue ? AppStrings.userName : AppStrings.enterEmail;

  String get passwordFormLabel =>
      isSignIn.isTrue ? AppStrings.password : AppStrings.enterPassword;

  String get switchTitle =>
      isSignIn.isTrue ? AppStrings.signUp : AppStrings.signIn;

  String get switchDescription => isSignIn.isTrue
      ? AppStrings.dontHaveAccount
      : AppStrings.alreadyHaveAccount;
}

import 'package:get/get.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/auth/auth_controller.dart';

class AuthBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthController());
  }
}
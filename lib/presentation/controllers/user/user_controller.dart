import 'package:get/state_manager.dart';
import 'package:scale3c_flutter_test_task/data/models/user/user.dart';
import 'package:scale3c_flutter_test_task/data/repositories/user.dart';

class UserController extends GetxController with StateMixin {
  final userRepository = FirestoreUserRepository();

  late final Rx<UserModel> user;

  @override
  void onInit() async {
    super.onInit();
    await getUserData();
  }

  Future<void> getUserData() async {
    change(null, status: RxStatus.loading());
    await userRepository.get().then(
          (value) => user = value.obs,
          onError: (_) => change(null, status: RxStatus.error()),
        );
    change(null, status: RxStatus.success());
  }

  Future<void> updateUser(UserModel updatedUser) async {
    await userRepository.set(updatedUser);
    user(updatedUser);
  }
}

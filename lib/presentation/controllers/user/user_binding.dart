import 'package:get/get.dart';
import 'package:scale3c_flutter_test_task/presentation/controllers/user/user_controller.dart';

class UserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserController());
  }
}
